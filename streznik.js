var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/api/prijava', function(req, res) {
	var uporabniskoIme = req.query
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var napaka = "";
	if (uporabniskoIme == null || uporabniskoIme.length == 0 || geslo == null || geslo.length == 0){
		napaka = "Napačna zahteva.";
		
	}
	else if (!uspesno) {
		napaka = "Avtentikacija ni uspela.";
	}

	res.send({status: uspesno, napaka: napaka});
});


app.get('/prijava', function(req, res) {
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var odgovor = "<html><head><title>" + (uspesno == true ? "Uspešno" : "Napaka") + "</title><style>body {padding:10px}</style></head>";
	odgovor += "<body>";
	if (uspesno)
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> uspešno prijavljen v sistem!</p>";
	else
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> nima pravice prijave v sistem!</p>";

	odgovor += "</body></html>";
	res.send(odgovor);
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/" + "uporabniki_streznik.json").toString());


function preveriSpomin(uporabniskoIme, geslo) {
	for (var i in podatkiSpomin) {
		var username = podatkiSpomin[i].split("/")[0];
		var password = podatkiSpomin[i].split("/")[1];
		if (uporabniskoIme == username && geslo == password){
			
			return true;}
	}
	return false;
}


function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (var i in podatkiDatotekaStreznik) {
		var username = podatkiDatotekaStreznik[i]["uporabnik"];
		var password = podatkiDatotekaStreznik[i]["geslo"];
		if (uporabniskoIme == username && geslo == password)
			return true;
	}
	return false;
}